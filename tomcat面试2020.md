## Tomcat是什么？

Tomcat 服务器Apache软件基金会项目中的一个核心项目，是一个免费的开放源代码的Web 应用服务器，属于轻量级应用服务器，在中小型系统和并发访问用户不是很多的场合下被普遍使用，是开发和调试JSP 程序的首选。

## Tomcat的缺省端口是多少，怎么修改

1. 找到Tomcat目录下的conf文件夹
2. 进入conf文件夹里面找到server.xml文件
3. 打开server.xml文件
4. 在server.xml文件里面找到下列信息
5. 把Connector标签的8080端口改成你想要的端口

```xml
<Service name="Catalina">
<Connector port="8080" protocol="HTTP/1.1"  connectionTimeout="20000"      redirectPort="8443" />
```

## tomcat 有哪几种Connector 运行模式(优化)？

下面，我们先大致了解Tomcat Connector的三种运行模式。

- **BIO：同步并阻塞** 一个线程处理一个请求。缺点：并发量高时，线程数较多，浪费资源。Tomcat7或以下，在Linux系统中默认使用这种方式。

 **配制项**：protocol=”HTTP/1.1”

- NIO：同步非阻塞IO

  利用Java的异步IO处理，可以通过少量的线程处理大量的请求，可以复用同一个线程处理多个connection(多路复用)。

  Tomcat8在Linux系统中默认使用这种方式。

  Tomcat7必须修改Connector配置来启动。

  **配制项**：protocol=”org.apache.coyote.http11.Http11NioProtocol”

  **备注**：我们常用的Jetty，Mina，ZooKeeper等都是基于java nio实现.

- APR：即Apache Portable Runtime，从操作系统层面解决io阻塞问题。**AIO方式，****异步非阻塞IO**(Java NIO2又叫AIO) 主要与NIO的区别主要是操作系统的底层区别.可以做个比喻:比作快递，NIO就是网购后要自己到官网查下快递是否已经到了(可能是多次)，然后自己去取快递；AIO就是快递员送货上门了(不用关注快递进度)。

  **配制项**：protocol=”org.apache.coyote.http11.Http11AprProtocol”

  **备注**：需在本地服务器安装APR库。Tomcat7或Tomcat8在Win7或以上的系统中启动默认使用这种方式。Linux如果安装了apr和native，Tomcat直接启动就支持apr。

## Tomcat有几种部署方式？

**在Tomcat中部署Web应用的方式主要有如下几种：**

1. 利用Tomcat的自动部署。

   把web应用拷贝到webapps目录。Tomcat在启动时会加载目录下的应用，并将编译后的结果放入work目录下。

2. 使用Manager App控制台部署。

   在tomcat主页点击“Manager App” 进入应用管理控制台，可以指定一个web应用的路径或war文件。

3. 修改conf/server.xml文件部署。

   修改conf/server.xml文件，增加Context节点可以部署应用。

4. 增加自定义的Web部署文件。

   在conf/Catalina/localhost/ 路径下增加 xyz.xml文件，内容是Context节点，可以部署应用。

## tomcat容器是如何创建servlet类实例？用到了什么原理？

1. 当容器启动时，会读取在webapps目录下所有的web应用中的web.xml文件，然后对 **xml文件进行解析，并读取servlet注册信息**。然后，将每个应用中注册的servlet类都进行加载，并通过 **反射的方式实例化**。（有时候也是在第一次请求时实例化）
2. 在servlet注册时加上1如果为正数，则在一开始就实例化，如果不写或为负数，则第一次请求实例化。

## Tomcat工作模式

Tomcat作为servlet容器，有三种工作模式：

- 1、独立的servlet容器，servlet容器是web服务器的一部分；
- 2、进程内的servlet容器，servlet容器是作为web服务器的插件和java容器的实现，web服务器插件在内部地址空间打开一个jvm使得java容器在内部得以运行。反应速度快但伸缩性不足；
- 3、进程外的servlet容器，servlet容器运行于web服务器之外的地址空间，并作为web服务器的插件和java容器实现的结合。反应时间不如进程内但伸缩性和稳定性比进程内优；

进入Tomcat的请求可以根据Tomcat的工作模式分为如下两类：

- Tomcat作为应用程序服务器：请求来自于前端的web服务器，这可能是Apache, IIS, Nginx等；
- Tomcat作为独立服务器：请求来自于web浏览器；

面试时问到Tomcat相关问题的几率并不高，正式因为如此，很多人忽略了对Tomcat相关技能的掌握，下面这一篇文章整理了Tomcat相关的系统架构，介绍了Server、Service、Connector、Container之间的关系，各个模块的功能，可以说把这几个掌握住了，Tomcat相关的面试题你就不会有任何问题了！另外，在面试的时候你还要有意识无意识的往Tomcat这个地方引，就比如说常见的Spring MVC的执行流程，一个URL的完整调用链路，这些相关的题目你是可以往Tomcat处理请求的这个过程去说的！掌握了Tomcat这些技能，面试官一定会佩服你的！

学了本章之后你应该明白的是：

- Server、Service、Connector、Container四大组件之间的关系和联系，以及他们的主要功能点；
- Tomcat执行的整体架构，请求是如何被一步步处理的；
- Engine、Host、Context、Wrapper相关的概念关系；
- Container是如何处理请求的；
- Tomcat用到的相关设计模式；

## Tomcat顶层架构

俗话说，站在巨人的肩膀上看世界，一般学习的时候也是先总览一下整体，然后逐个部分个个击破，最后形成思路，了解具体细节，Tomcat的结构很复杂，但是 Tomcat 非常的模块化，找到了 Tomcat 最核心的模块，问题才可以游刃而解，了解了 Tomcat 的整体架构对以后深入了解 Tomcat 来说至关重要！

先上一张Tomcat的顶层结构图（图A），如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215330153.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

Tomcat中最顶层的容器是Server，代表着整个服务器，从上图中可以看出，一个Server可以包含至少一个Service，即可以包含多个Service，用于具体提供服务。

Service主要包含两个部分：Connector和Container。从上图中可以看出 Tomcat 的心脏就是这两个组件，他们的作用如下：

- Connector用于处理连接相关的事情，并提供Socket与Request请求和Response响应相关的转化;
- Container用于封装和管理Servlet，以及具体处理Request请求；

一个Tomcat中只有一个Server，一个Server可以包含多个Service，一个Service只有一个Container，但是可以有多个Connectors，这是因为一个服务可以有多个连接，如同时提供Http和Https链接，也可以提供向相同协议不同端口的连接，示意图如下（Engine、Host、Context下面会说到）：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215344811.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

多个 Connector 和一个 Container 就形成了一个 Service，有了 Service 就可以对外提供服务了，但是 Service 还要一个生存的环境，必须要有人能够给她生命、掌握其生死大权，那就非 Server 莫属了！所以整个 Tomcat 的生命周期由 Server 控制。

另外，上述的包含关系或者说是父子关系，都可以在tomcat的conf目录下的server.xml配置文件中看出，下图是删除了注释内容之后的一个完整的server.xml配置文件（Tomcat版本为8.0）

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215355649.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

详细的配置文件内容可以到Tomcat官网查看：[Tomcat配置文件](http://tomcat.apache.org/tomcat-8.0-doc/index.html)

上边的配置文件，还可以通过下边的一张结构图更清楚的理解：

![在这里插入图片描述](https://img-blog.csdnimg.cn/2019102121541531.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

Server标签设置的端口号为8005，shutdown=”SHUTDOWN” ，表示在8005端口监听“SHUTDOWN”命令，如果接收到了就会关闭Tomcat。一个Server有一个Service，当然还可以进行配置，一个Service有多个Connector，Service左边的内容都属于Container的，Service下边是Connector。

### Tomcat顶层架构小结

1. Tomcat中只有一个Server，一个Server可以有多个Service，一个Service可以有多个Connector和一个Container；
2. Server掌管着整个Tomcat的生死大权；
3. Service 是对外提供服务的；
4. Connector用于接受请求并将请求封装成Request和Response来具体处理；
5. Container用于封装和管理Servlet，以及具体处理request请求；

知道了整个Tomcat顶层的分层架构和各个组件之间的关系以及作用，对于绝大多数的开发人员来说Server和Service对我们来说确实很远，而我们开发中绝大部分进行配置的内容是属于Connector和Container的，所以接下来介绍一下Connector和Container。

## Connector和Container的微妙关系

由上述内容我们大致可以知道一个请求发送到Tomcat之后，首先经过Service然后会交给我们的Connector，Connector用于接收请求并将接收的请求封装为Request和Response来具体处理，Request和Response封装完之后再交由Container进行处理，Container处理完请求之后再返回给Connector，最后在由Connector通过Socket将处理的结果返回给客户端，这样整个请求的就处理完了！

Connector最底层使用的是Socket来进行连接的，Request和Response是按照HTTP协议来封装的，所以Connector同时需要实现TCP/IP协议和HTTP协议！

Tomcat既然需要处理请求，那么肯定需要先接收到这个请求，接收请求这个东西我们首先就需要看一下Connector！

Connector架构分析

Connector用于接受请求并将请求封装成Request和Response，然后交给Container进行处理，Container处理完之后在交给Connector返回给客户端。

因此，我们可以把Connector分为四个方面进行理解：

1. Connector如何接受请求的？
2. 如何将请求封装成Request和Response的？
3. 封装完之后的Request和Response如何交给Container进行处理的？
4. Container处理完之后如何交给Connector并返回给客户端的？

首先看一下Connector的结构图（图B），如下所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215430677.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

Connector就是使用ProtocolHandler来处理请求的，不同的ProtocolHandler代表不同的连接类型，比如：Http11Protocol使用的是普通Socket来连接的，Http11NioProtocol使用的是NioSocket来连接的。

其中ProtocolHandler由包含了三个部件：Endpoint、Processor、Adapter。

1. Endpoint用来处理底层Socket的网络连接，Processor用于将Endpoint接收到的Socket封装成Request，Adapter用于将Request交给Container进行具体的处理。
2. Endpoint由于是处理底层的Socket网络连接，因此Endpoint是用来实现TCP/IP协议的，而Processor用来实现HTTP协议的，Adapter将请求适配到Servlet容器进行具体的处理。
3. Endpoint的抽象实现AbstractEndpoint里面定义的Acceptor和AsyncTimeout两个内部类和一个Handler接口。Acceptor用于监听请求，AsyncTimeout用于检查异步Request的超时，Handler用于处理接收到的Socket，在内部调用Processor进行处理。

至此，我们应该很轻松的回答1，2，3的问题了，但是4还是不知道，那么我们就来看一下Container是如何进行处理的以及处理完之后是如何将处理完的结果返回给Connector的？

## Container架构分析

Container用于封装和管理Servlet，以及具体处理Request请求，在Container内部包含了4个子容器，结构图如下（图C）：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215443306.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

4个子容器的作用分别是：

1. Engine：引擎，用来管理多个站点，一个Service最多只能有一个Engine；
2. Host：代表一个站点，也可以叫虚拟主机，通过配置Host就可以添加站点；
3. Context：代表一个应用程序，对应着平时开发的一套程序，或者一个WEB-INF目录以及下面的web.xml文件；
4. Wrapper：每一Wrapper封装着一个Servlet；

下面找一个Tomcat的文件目录对照一下，如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215455991.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

Context和Host的区别是Context表示一个应用，我们的Tomcat中默认的配置下webapps下的每一个文件夹目录都是一个Context，其中ROOT目录中存放着主应用，其他目录存放着子应用，而整个webapps就是一个Host站点。

我们访问应用Context的时候，如果是ROOT下的则直接使用域名就可以访问，例如：www.baidu.com，如果是Host（webapps）下的其他应用，则可以使用www.baidu.com/docs进行访问，当然默认指定的根应用（ROOT）是可以进行设定的，只不过Host站点下默认的主应用是ROOT目录下的。

看到这里我们知道Container是什么，但是还是不知道Container是如何进行请求处理的以及处理完之后是如何将处理完的结果返回给Connector的？别急！下边就开始探讨一下Container是如何进行处理的！

### Container如何处理请求的

Container处理请求是使用Pipeline-Valve管道来处理的！（Valve是阀门之意）

Pipeline-Valve是**责任链模式**，责任链模式是指在一个请求处理的过程中有很多处理者依次对请求进行处理，每个处理者负责做自己相应的处理，处理完之后将处理后的结果返回，再让下一个处理者继续处理。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215507725.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

但是！Pipeline-Valve使用的责任链模式和普通的责任链模式有些不同！区别主要有以下两点：

- 每个Pipeline都有特定的Valve，而且是在管道的最后一个执行，这个Valve叫做BaseValve，BaseValve是不可删除的；
- 在上层容器的管道的BaseValve中会调用下层容器的管道。

我们知道Container包含四个子容器，而这四个子容器对应的BaseValve分别在：StandardEngineValve、StandardHostValve、StandardContextValve、StandardWrapperValve。

Pipeline的处理流程图如下（图D）：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191021215519408.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoaW5rV29u,size_16,color_FFFFFF,t_70)

- Connector在接收到请求后会首先调用最顶层容器的Pipeline来处理，这里的最顶层容器的Pipeline就是EnginePipeline（Engine的管道）；
- 在Engine的管道中依次会执行EngineValve1、EngineValve2等等，最后会执行StandardEngineValve，在StandardEngineValve中会调用Host管道，然后再依次执行Host的HostValve1、HostValve2等，最后在执行StandardHostValve，然后再依次调用Context的管道和Wrapper的管道，最后执行到StandardWrapperValve。
- 当执行到StandardWrapperValve的时候，会在StandardWrapperValve中创建FilterChain，并调用其doFilter方法来处理请求，这个FilterChain包含着我们配置的与请求相匹配的Filter和Servlet，其doFilter方法会依次调用所有的Filter的doFilter方法和Servlet的service方法，这样请求就得到了处理！
- 当所有的Pipeline-Valve都执行完之后，并且处理完了具体的请求，这个时候就可以将返回的结果交给Connector了，Connector在通过Socket的方式将结果返回给客户端。

## 总结

至此，我们已经对Tomcat的整体架构有了大致的了解，从图A、B、C、D可以看出来每一个组件的基本要素和作用。我们在脑海里应该有一个大概的轮廓了！如果你面试的时候，让你简单的聊一下Tomcat，上面的内容你能脱口而出吗？当你能够脱口而出的时候，面试官一定会对你刮目相看的！



## tomcat优化

### 1、你怎样给 tomcat 去调优?

2. JVM 参数调优：-Xms<size> 表示 JVM 初始化堆的大小，-Xmx<size>表示 JVM 堆的最
    大值。这两个值的大小一般根据需要进行设置。当应用程序需要的内存超出堆的最大值时虚拟
    机就会提示内存溢出，并且导致应用服务崩溃。因此一般建议堆的最大值设置为可用内存的最
    大值的 80%。在 catalina.bat 中，设置 JAVA_OPTS='-Xms256m -Xmx512m'，表示初始化
    内存为 256MB，可以使用的最大内存为 512MB。

3. 禁用 DNS 查询
    当 web 应用程序向要记录客户端的信息时，它也会记录客户端的 IP 地址或者通过域名服务
    器查找机器名转换为 IP 地址。DNS 查询需要占用网络，并且包括可能从很多很远的服务器或
    者不起作用的服务器上去获取对应的 IP 的过程，这样会消耗一定的时间。为了消除 DNS 查询
    对性能的影响我们可以关闭 DNS 查询，方式是修改 server.xml 文件中的 enableLookups 参
    数值：
    Tomcat4

  ```xml
  <Connector className="org.apache.coyote.tomcat4.CoyoteConnector" port="80"
  minProcessors="5" maxProcessors="75" enableLookups="false" redirectPort="8443"
  acceptCount="100" debug="0" connectionTimeout="20000"
  useURIValidationHack="false" disableUploadTimeout="true" />
  ```

  Tomcat5

  ```xml
  <Connector port="80" maxThreads="150" minSpareThreads="25"
  maxSpareThreads="75" enableLookups="false" redirectPort="8443"
  acceptCount="100" debug="0" connectionTimeout="20000"
  disableUploadTimeout="true"/>
  ```

  

4. 调整线程数
    通过应用程序的连接器（Connector）进行性能控制的的参数是创建的处理请求的线程数。
    Tomcat 使用线程池加速响应速度来处理请求。在 Java 中线程是程序运行时的路径，是在一个
    程序中与其它控制线程无关的、能够独立运行的代码段。它们共享相同的地址空间。多线程帮
    助程序员写出 CPU 最大利用率的高效程序，使空闲时间保持最低，从而接受更多的请求。
    Tomcat4 中可以通过修改 minProcessors 和 maxProcessors 的值来控制线程数。这些值
    在安装后就已经设定为默认值并且是足够使用的，但是随着站点的扩容而改大这些值。
    minProcessors 服务器启动时创建的处理请求的线程数应该足够处理一个小量的负载。也就是
    说，如果一天内每秒仅发生 5 次单击事件，并且每个请求任务处理需要 1 秒钟，那么预先设置
    线程数为 5 就足够了。但在你的站点访问量较大时就需要设置更大的线程数，指定为参数
    maxProcessors 的值。maxProcessors 的值也是有上限的，应防止流量不可控制（或者恶意
    的服务攻击），从而导致超出了虚拟机使用内存的大小。如果要加大并发连接数，应同时加大
    这两个参数。web server 允许的最大连接数还受制于操作系统的内核参数设置，通常
    Windows 是 2000 个左右，Linux 是 1000 个左右。
    在 Tomcat5 对这些参数进行了调整，请看下面属性：
    maxThreads Tomcat 使用线程来处理接收的每个请求。这个值表示 Tomcat 可创建的最大
    的线程数。
    acceptCount 指定当所有可以使用的处理请求的线程数都被使用时，可以放到处理队列中的
    请求数，超过这个数的请求将不予处理。
    connnectionTimeout 网络连接超时，单位：毫秒。设置为 0 表示永不超时，这样设置有隐
    患的。通常可设置为 30000 毫秒。
    minSpareThreads Tomcat 初始化时创建的线程数。
    maxSpareThreads 一旦创建的线程超过这个值，Tomcat 就会关闭不再需要的 socket 线
    程。
    最好的方式是多设置几次并且进行测试，观察响应时间和内存使用情况。在不同的机器、
    操作系统或虚拟机组合的情况下可能会不同，而且并不是所有人的 web 站点的流量都是一样
    的，因此没有一刀切的方案来确定线程数的值。

5. 如何加大 tomcat 连接数
    在 tomcat 配置文件 server.xml 中的<Connector />配置中，和连接数相关的参数有：
    minProcessors：最小空闲连接线程数，用于提高系统处理性能，默认值为 10
    maxProcessors：最大连接线程数，即：并发处理的最大请求数，默认值为 75
    acceptCount：允许的最大连接数，应大于等于 maxProcessors，默认值为 100
    enableLookups：是否反查域名，取值为：true 或 false。为了提高处理能力，应设置为 false
    connectionTimeout：网络连接超时，单位：毫秒。设置为 0 表示永不超时，这样设置有隐患
    的。通常可设置为 30000 毫秒。
    其中和最大连接数相关的参数为 maxProcessors 和 acceptCount。如果要加大并发连接数，
    应同时加大这两个参数。
    web server 允许的最大连接数还受制于操作系统的内核参数设置，通常 Windows 是 2000 个
    左右，Linux 是 1000 个左右。tomcat5 中的配置示例：

  ```xml
  <Connector port="8080"
  maxThreads="150" minSpareThreads="25" maxSpareThreads="75"
  enableLookups="false" redirectPort="8443" acceptCount="100"
  debug="0" connectionTimeout="20000"
  disableUploadTimeout="true" />
  ```

  对于其他端口的侦听配置，以此类推。

6. tomcat 中如何禁止列目录下的文件
    在{tomcat_home}/conf/web.xml 中，把 listings 参数设置成 false 即可，如下：

  ```xml
  <init-param>
  <param-name>listings</param-name>
  <param-value>false</param-value>
  </init-param>
  <init-param>
  <param-name>listings</param-name>
  <param-value>false</param-value>
  </init-param>
  ```

  4.怎样加大 tomcat 的内存。
  首先检查程序有没有限入死循环
  这个问题主要还是由这个问题 java.lang.OutOfMemoryError: Java heap space 引起的。第一次出现这样
  的的问题以后，引发了其他的问题。在网上一查可能是 JAVA 的堆栈设置太小的原因。
  跟据网上的答案大致有这两种解决方法：
  1、设置环境变量
  解决方法：手动设置 Heap size
  修改 TOMCAT_HOME/bin/catalina.sh
  set JAVA_OPTS= -Xms32m -Xmx512m
  可以根据自己机器的内存进行更改。
  2、java -Xms32m -Xmx800m className
  就是在执行 JAVA 类文件时加上这个参数，其中 className 是需要执行的确类名。（包括包名）
  这个解决问题了。而且执行的速度比没有设置的时候快很多。
  如果在测试的时候可能会用 Eclispe 这时候就需要在 Eclipse ->run -arguments 中的 VM arguments 中输
  入-Xms32m -Xmx800m 这个参数就可以了。
  后来在 Eclilpse 中修改了启动参数，在 VM arguments 加入了-Xms32m -Xmx800m，问题解决。
  一、java.lang.OutOfMemoryError: PermGen space
  PermGen space 的全称是 Permanent Generation space,是指内存的永久保存区域,
  这块内存主要是被 JVM 存放 Class 和 Meta 信息的,Class 在被 Loader 时就会被放到 PermGen space 中,
  它和存放类实例(Instance)的 Heap 区域不同,GC(Garbage Collection)不会在主程序运行期对
  PermGen space 进行清理，所以如果你的应用中有很多 CLASS 的话,就很可能出现 PermGen space 错
  误,
  这种错误常见在 web 服务器对 JSP 进行 pre compile 的时候。如果你的 WEB APP 下都用了大量的第三
  方 jar, 其大小
  超过了 jvm 默认的大小(4M)那么就会产生此错误信息了。
  解决方法： 手动设置 MaxPermSize 大小
  修改 TOMCAT_HOME/bin/catalina.sh
  在“echo "Using CATALINA_BASE: $CATALINA_BASE"”上面加入以下行：
  JAVA_OPTS="-server -XX:PermSize=64M -XX:MaxPermSize=128m
  建议：将相同的第三方 jar 文件移置到 tomcat/shared/lib 目录下，这样可以达到减少 jar 文档重复占用内
  存的目的。
  ###  二、java.lang.OutOfMemoryError: Java heap space
  Heap size 设置
  JVM 堆的设置是指 java 程序运行过程中 JVM 可以调配使用的内存空间的设置.JVM 在启动的时候会自动
  设置 Heap size 的值，
  其初始空间(即-Xms)是物理内存的 1/64，最大空间(-Xmx)是物理内存的 1/4。可以利用 JVM 提供的-Xmn
  -Xms -Xmx 等选项可
  进行设置。Heap size 的大小是 Young Generation 和 Tenured Generaion 之和。
  提示：在 JVM 中如果 98％的时间是用于 GC 且可用的 Heap size 不足 2％的时候将抛出此异常信息。
  提示：Heap Size 最大不要超过可用物理内存的 80％，一般的要将-Xms 和-Xmx 选项设置为相同，而-
  Xmn 为 1/4 的-Xmx 值。
  解决方法：手动设置 Heap size
  修改 TOMCAT_HOME/bin/catalina.sh
  在“echo "Using CATALINA_BASE: $CATALINA_BASE"”上面加入以下行：
  JAVA_OPTS="-server -Xms800m -Xmx800m -XX:MaxNewSize=256m"
  三、实例，以下给出 1G 内存环境下 java jvm 的参数设置参考：
  JAVA_OPTS="-server -Xms800m -Xmx800m -XX:PermSize=64M -XX:MaxNewSize=256m -
  XX:MaxPermSize=128m -Djava.awt.headless=true "
  很大的 web 工程，用 tomcat 默认分配的内存空间无法启动，如果不是在 myeclipse 中启动 tomcat 可以
  对 tomcat 这样设置：
  TOMCAT_HOME/bin/catalina.bat 中添加这样一句话：
  set JAVA_OPTS=-server -Xms2048m -Xmx4096m -XX:PermSize=512M -
  XX:MaxPermSize=1024M -Duser.timezone=GMT+08
  或者
  set JAVA_OPTS= -Xmx1024M -Xms512M -XX:MaxPermSize=256m
  如果要在 myeclipse 中启动，上述的修改就不起作用了，可如下设置：
  Myeclipse->preferences->myeclipse->servers->tomcat->tomcat×.×->JDK 面板中的
  Optional Java VM arguments 中添加：-Xmx1024M -Xms512M -XX:MaxPermSize=256m
  以上是转贴，但本人遇见的问题是：在 myeclipse 中启动 Tomcat 时，提示"ava.lang.OutOfMemoryError:
  Java heap space"，解决办法就是：
  Myeclipse->preferences->myeclipse->servers->tomcat->tomcat×.×->JDK 面板中的
  Optional Java VM arguments 中添加：-Xmx1024M -Xms512M -XX:MaxPermSize=256m
  5.Tomcat 有几种部署方式
  tomcat 中四种部署项目的方法
  第一种方法：
  在 tomcat 中的 conf 目录中，在 server.xml 中的，<host/>节点中添加：
  <Context path="/hello"
  docBase="D:/eclipse3.2.2/forwebtoolsworkspacehello/WebRoot" debug="0"
  privileged="true">
  </Context>
  至于 Context 节点属性，可详细见相关文档。
  第二种方法：
  将 web 项目文件件拷贝到 webapps 目录中。
  第三种方法：
  很灵活，在 conf 目录中，新建 Catalina（注意大小写）＼localhost 目录，在该目录中新建一
  个 xml 文件，名字可以随意取，只要和当前文件中的文件名不重复就行了，该 xml 文件的内容
  为：
  <Context path="/hello" docBase="D:eclipse3.2.2forwebtoolsworkspacehelloWebRoot"
  debug="0" privileged="true">
  </Context>
  第 3 个方法有个优点，可以定义别名。服务器端运行的项目名称为 path，外部访问的 URL 则
  使用 XML 的文件名。这个方法很方便的隐藏了项目的名称，对一些项目名称被固定不能更
  换，但外部访问时又想换个路径，非常有效。
  第 2、3 还有优点，可以定义一些个性配置，如数据源的配置等。
  第四种办法,:
  可以用 tomcat 在线后台管理器,一般 tomcat 都打开了,直接上传 war 就可以
  6.Tomcat 的优化经验。
  Tomcat 作为 Web 服务器，它的处理性能直接关系到用户体验，下面是几种常见的
  优化措施:
  
  去掉对 web.xml 的监视，把 jsp 提前编辑成 Servlet。有富余物理内存的
  情况，加大 tomcat 使用的 jvm 的内存。
  
  服务器资源
  服务器所能提供 CPU、内存、硬盘的性能对处理能力有决定性影响。
  o 对于高并发情况下会有大量的运算，那么 CPU 的速度会直接影响到处
  理速度。
  o 内存在大量数据处理的情况下，将会有较大的内存容量需求，可以用 -
  Xmx -Xms -XX:MaxPermSize 等参数对内存不同功能块进行划分。我
  们之前就遇到过内存分配不足，导致虚拟机一直处于 full GC，从而导
  致处理能力严重下降。
  o 硬盘主要问题就是读写性能，当大量文件进行读写时，磁盘极容易成为
  性能瓶颈。最好的办法还是利用下面提到的缓存。
  
  利用缓存和压缩
  对于静态页面最好是能够缓存起来，这样就不必每次从磁盘上读。这里我们采
  用了 Nginx 作为缓存服务器，将图片、css、js 文件都进行了缓存，有效的
  减少了后端 tomcat 的访问。 另外，为了能加快网络传输速度，开启
  gzip 压缩也是必不可少的。但考虑到 tomcat 已经需要处理很多东西了，所
  以把这个压缩的工作就交给前端的 Nginx 来完成。 除了文本可以用
  gzip 压缩，其实很多图片也可以用图像处理工具预先进行压缩，找到一个平
  衡点可以让画质损失很小而文件可以减小很多。曾经我就见过一个图片从 300
  多 kb 压缩到几十 kb，自己几乎看不出来区别。

  采用集群
  单个服务器性能总是有限的，最好的办法自然是实现横向扩展，那么组建
  tomcat 集群是有效提升性能的手段。我们还是采用了 Nginx 来作为请求分
  流的服务器，后端多个 tomcat 共享 session 来协同工作。可以参考之前写
  的《利用 nginx+tomcat+memcached 组建 web 服务器负载均衡》。

  优化 tomcat 参数
  这里以 tomcat7 的参数配置为例，需要修改 conf/server.xml 文件，主要是优化连
  接配置，关闭客户端 dns 查询。

  ```
  <Connector port="8080"
  protocol="org.apache.coyote.http11.Http11NioProtocol"
  connectionTimeout="20000"
  redirectPort="8443"
  maxThreads="500"
  minSpareThreads="20"
  acceptCount="100"
  disableUploadTimeout="true"
  enableLookups="false"
  URIEncoding="UTF-8" />
  ```

  